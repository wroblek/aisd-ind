/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivery;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Krzysztof
 */
public class Car implements Runnable {

    private final ArrayList<Order> orders;
    private ArrayList<Vertex> path;
    private boolean running = false;
    private int capacity = 0;
    private final Delivery d;
    private int id = 0;
    private final MyLogger logger;

    public Car(int id, int cap, Delivery d) {
        capacity = cap;
        orders = new ArrayList();
        path = null;
        this.d = d;
        this.id = id;
        logger = MyLogger.getLogger();
    }

    public void setPath(ArrayList<Vertex> p) {
        path = p;
    }

    public void addOrder(Order o) {
        orders.add(o);
        StringBuilder sb = new StringBuilder();
        sb.append("Samochod ");
        sb.append(id);
        sb.append(" - pobrano przesylke ");
        sb.append(o.getId());
        sb.append(" - z ");
        sb.append(o.getSource().getName());
        logger.log(sb.toString());
    }

    public boolean isFull() {
        return orders.size() == capacity;
    }

    public boolean isEmpty() {
        return orders.isEmpty();
    }

    public ArrayList<Vertex> getPath() {
        return path;
    }
    
    @Override
    public String toString(){
        return Integer.toString(id);
    }

    @Override
    public void run() {
        if (!running && path != null) {
            try {
                running = true;
                StringBuilder sb = new StringBuilder();
                sb.append("Samochod ");
                sb.append(id);
                sb.append(" - wyjazd");
                logger.log(sb.toString());

                for (Iterator<Order> it = orders.iterator(); it.hasNext();) {
                    Order o = it.next();
                    if (o.getDest() == o.getSource()) {
                        it.remove();
                        sb = new StringBuilder();
                        sb.append("Samochod ");
                        sb.append(id);
                        sb.append(" - dostarczono przesylke ");
                        sb.append(o.getId());
                        sb.append(" - do ");
                        sb.append(o.getDest().getName());
                        logger.log(sb.toString());
                    }
                }

                int timeToReturn = 0;
                for (int i = 1; i < path.size(); i++) {
                    timeToReturn += path.get(i - 1).distanceTo(path.get(i));
                    logger.dispatchCar(this, path.get(i-1), path.get(i),path.get(i - 1).distanceTo(path.get(i)));
                    Thread.sleep(path.get(i - 1).distanceTo(path.get(i)));

                    for (Iterator<Order> it = orders.iterator(); it.hasNext();) {
                        Order o = it.next();
                        if (o.getDest() == path.get(i)) {
                            it.remove();
                            sb = new StringBuilder();
                            sb.append("Samochod ");
                            sb.append(id);
                            sb.append(" - dostarczono przesylke ");
                            sb.append(o.getId());
                            sb.append(" - do ");
                            sb.append(o.getDest().getName());
                            logger.log(sb.toString());
                        }
                    }
                }
                
               
                for (int i = path.size()-1; i > 0; i--) {
                    logger.dispatchCar(this, path.get(i), path.get(i-1),path.get(i).distanceTo(path.get(i-1)));
                    Thread.sleep(path.get(i).distanceTo(path.get(i-1)));
                }
                sb = new StringBuilder();
                sb.append("Samochod ");
                sb.append(id);
                sb.append(" - powrót");
                logger.log(sb.toString());

                Car c = this;
                new Thread(
                        new Runnable() {
                            @Override
                            public void run() {
                                d.carReadyToLoad(c);
                            }
                        }
                ).start();
                running = false;
            } catch (InterruptedException e) {

            }
        }
    }

}
