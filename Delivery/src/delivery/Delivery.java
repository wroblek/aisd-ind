/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivery;

import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Krzysztof
 */
public class Delivery {
    
    private final int MULTIPLIER = 50;

    private final Queue<Car> toLoad;
    String mapFile;
    String ordersFile;
    int carCount;
    int carCapacity;
    ArrayList<Vertex> vList;
    PriorityQueue<Order> oQueue;
    ArrayList<Car> cars;
    Vertex source;
    PathFinder pf;
    MyLogger logger;
    VisualizationViewer<String, String> vv ;
    

    public Delivery(String map, String orders, int carCount, int carCapacity) {
        this.mapFile = map;
        this.ordersFile = orders;
        this.carCount = carCount;
        this.carCapacity = carCapacity;
        vList = new ArrayList();
        cars = new ArrayList();
        oQueue = new PriorityQueue();
        toLoad = new LinkedList();
        pf = new DijkstraPathFinder();
        logger = MyLogger.getLogger();
    }

    public void start() {
        try {
            
            
        UndirectedGraph<String, String> graph = new UndirectedSparseGraph<String, String>();
            
            BufferedReader in = new BufferedReader(new FileReader(new File(mapFile)));
            logger.log("Start");
            in.readLine();
            for (String line = in.readLine(); !line.equals(""); line = in.readLine()) {
                Vertex v = new Vertex(line.split(" ")[1], Integer.parseInt(line.split(" ")[0]));
                vList.add(v);
                graph.addVertex(line.split(" ")[1]);
            }
            in.readLine();
            
            Integer counter = 1;
            for (String line = in.readLine(); line != null; line = in.readLine()) {
                String[] tokens = line.split(" ");
                vList.get(Integer.parseInt(tokens[0])).addAdjacence(
                        new Edge(vList.get(Integer.parseInt(tokens[1])),
                                Integer.parseInt(tokens[2])*MULTIPLIER));
                vList.get(Integer.parseInt(tokens[1])).addAdjacence(
                        new Edge(vList.get(Integer.parseInt(tokens[0])),
                                Integer.parseInt(tokens[2])*MULTIPLIER));
                StringBuilder sbl = new StringBuilder();
                sbl.append(tokens[2]);
                sbl.append(" (");
                sbl.append(counter++);
                sbl.append(")");
                graph.addEdge(sbl.toString(), vList.get(Integer.parseInt(tokens[0])).getName(), vList.get(Integer.parseInt(tokens[1])).getName());

            }
            in.close();

            in = new BufferedReader(new FileReader(new File(ordersFile)));
            source = vList.get(Integer.parseInt(in.readLine()));
            pf.init(source);
            for (String line = in.readLine(); line != null && !line.equals(""); line = in.readLine()) {
                String[] tokens = line.split(" ");
                StringBuilder name = new StringBuilder();
                for (int i = 3; i < tokens.length - 1; i++) {
                    name.append(tokens[i]);
                    name.append(" ");
                }

                int path = 0;
                ArrayList<Vertex> vl = pf.getShortestPathTo(vList.get(Integer.parseInt(tokens[2])));
                for (int i = 1; i < vl.size(); i++) {
                    path += vl.get(i - 1).distanceTo(vl.get(i));
                }

                Order o = new Order(
                        Integer.parseInt(tokens[0]),
                        vList.get(Integer.parseInt(tokens[1])),
                        vList.get(Integer.parseInt(tokens[2])),
                        name.toString().trim(),
                        Short.parseShort(tokens[tokens.length - 1]),
                        path
                );
                oQueue.add(o);
            }
            in.close();

            for (int i = 0; i < carCount; i++) {
                cars.add(new Car(i + 1, carCapacity, this));
            }

            logger.setGraph(graph);
            
            for (Car c : cars) {
                toLoad.add(c);
            }
            load();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Delivery.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Delivery.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void load() {
        synchronized (toLoad) {
            Car c;
            while ((c = toLoad.poll()) != null && !oQueue.isEmpty()) {
                ArrayList<Order> rejected = new ArrayList();
                while (!c.isFull() && !oQueue.isEmpty()) {
                    if (c.isEmpty()) {
                        Order o = oQueue.poll();
                        c.setPath(pf.getShortestPathTo(o.getDest()));
                        c.addOrder(o);
                    } else {
                        Order o = oQueue.poll();
                        if (c.getPath().contains(o.getDest())) {
                            c.addOrder(o);
                        } else {
                            rejected.add(o);
                        }
                    }
                }
                for (Order o : rejected) {
                    oQueue.add(o);
                }
                new Thread(c).start();
            }
        }
    }

    public void carReadyToLoad(Car c) {
        synchronized (toLoad) {
            toLoad.add(c);
        }
        load();
    }
}
