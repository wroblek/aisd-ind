/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;

/**
 *
 * @author Krzysztof
 */
public class DijkstraPathFinder implements PathFinder {

    @Override
    public void init(Vertex source) {
        source.setMinDistance(0);
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue();
        vertexQueue.add(source);

        while (!vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.poll();

            for (Edge e : u.getAdjacencies()) {
                Vertex v = e.target;
                int weight = e.weight;
                int distanceThroughU = u.getMinDistance() + weight;
                if (distanceThroughU < v.getMinDistance()) {
                    vertexQueue.remove(v);
                    v.setMinDistance(distanceThroughU);
                    v.setPreviousVertex(u);
                    vertexQueue.add(v);
                }
            }
        }
    }

    @Override
    public ArrayList<Vertex> getShortestPathTo(Vertex target) {
        ArrayList<Vertex> path = new ArrayList();
        for (Vertex vertex = target; vertex != null; vertex = vertex.getPreviousVertex()) {
            path.add(vertex);
        }
        Collections.reverse(path);
        return path;
    }

}
