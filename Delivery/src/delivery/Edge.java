/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivery;

/**
 *
 * @author Krzysztof
 */
public class Edge {

    public final Vertex target;
    public final int weight;

    public Edge(Vertex argTarget, int argWeight) {
        target = argTarget;
        weight = argWeight;
    }
}
