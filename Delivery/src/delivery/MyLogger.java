/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivery;

import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.MultiLayerTransformer;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JFrame;
import javax.swing.Timer;

/**
 *
 * @author Krzysztof
 */
public class MyLogger {

    private static MyLogger log = null;
    private long start = 0;
    private VisualizationViewer<String, String> vv = null;
    private UndirectedGraph<String, String> graph;
    private ArrayList<CarLocation> cars = new ArrayList<>();

    private MyLogger() {
        start = System.currentTimeMillis();
    }

    public void setGraph(UndirectedGraph graph) {
        this.graph = graph;
        JFrame f = new JFrame();
        vv
                = new VisualizationViewer<String, String>(
                        new CircleLayout<String, String>(graph));

        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<>());
        Timer timer = new Timer(40, new ActionListener() {
            long prevMillis = 0;

            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (this) {
                    if (prevMillis == 0) {
                        prevMillis = System.currentTimeMillis();
                    }
                    synchronized (cars) {
                        for (Iterator<CarLocation> it = cars.iterator(); it.hasNext();) {
                            CarLocation c = it.next();
                            if (c.elapsed > c.total) {
                                it.remove();
                                vv.removePostRenderPaintable(c.carp);
                            } else {
                                double phase = (new Long(c.elapsed)).doubleValue() / (new Long(c.total)).doubleValue();
                                c.carp.setLocation(phase);
                                c.elapsed += 50;
                            }
                        }
                    }
                    vv.repaint();
                }
            }
        }
        );

        timer.start();
        f.getContentPane()
                .add(vv);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        f.pack();

        f.setVisible(
                true);
    }

    public static MyLogger getLogger() {
        if (log == null) {
            log = new MyLogger();
        }
        return log;
    }

    public void dispatchCar(Car c, Vertex v1, Vertex v2, long time) {

        if (vv != null) {
            synchronized (cars) {
                String edge = graph.findEdge(v1.getName(), v2.getName());
                CarPainter cp = new CarPainter<String, String>(vv, edge, v1.getName(), c);
                vv.addPostRenderPaintable(cp);
                cars.add(new CarLocation(c, cp, time));
            }
        }

    }

    public void log(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(System.currentTimeMillis() - start);
        sb.append(": ");
        sb.append(s);
        System.out.println(sb.toString());

    }

    class CarPainter<V, E> implements VisualizationViewer.Paintable {

        private final VisualizationViewer<V, E> vv;
        private final E edge;
        private final Car car;
        private double location;
        private final V vs;

        CarPainter(
                VisualizationViewer<V, E> vv,
                E edge,
                V s,
                Car c) {
            this.vv = vv;
            this.edge = edge;
            this.car = c;
            this.vs = s;
        }

        public void setLocation(double location) {
            this.location = location;
        }

        @Override
        public void paint(Graphics gr) {
            Shape shape = getTransformedEdgeShape(vv, vv.getGraphLayout(), edge, vs);
            Point2D p = computePointAt(shape, 0.2, location);
            Color color = gr.getColor();
            gr.setColor(Color.BLUE);
            gr.drawString(car.toString(), (int) p.getX(), (int) p.getY());
            gr.setColor(color);
        }

        @Override
        public boolean useTransform() {
            return true;
        }

    }

    private double computeLength(Shape shape, double flatness) {
        double length = 0;
        PathIterator pi = shape.getPathIterator(null, flatness);
        double[] coords = new double[6];
        double previous[] = new double[2];
        while (!pi.isDone()) {
            int segment = pi.currentSegment(coords);
            switch (segment) {
                case PathIterator.SEG_MOVETO:
                    previous[0] = coords[0];
                    previous[1] = coords[1];
                    break;

                case PathIterator.SEG_LINETO:
                    double dx = previous[0] - coords[0];
                    double dy = previous[1] - coords[1];
                    length += Math.sqrt(dx * dx + dy * dy);
                    previous[0] = coords[0];
                    previous[1] = coords[1];
                    break;
            }
            pi.next();
        }
        return length;
    }

    public Point2D computePointAt(
            Shape shape, double flatness, double alpha) {
        alpha = Math.min(1.0, Math.max(0.0, alpha));
        double totalLength = computeLength(shape, flatness);
        double targetLength = alpha * totalLength;
        double currentLength = 0;
        PathIterator pi = shape.getPathIterator(null, flatness);
        double[] coords = new double[6];
        double previous[] = new double[2];
        while (!pi.isDone()) {
            int segment = pi.currentSegment(coords);
            switch (segment) {
                case PathIterator.SEG_MOVETO:
                    previous[0] = coords[0];
                    previous[1] = coords[1];
                    break;

                case PathIterator.SEG_LINETO:
                    double dx = previous[0] - coords[0];
                    double dy = previous[1] - coords[1];
                    double segmentLength = Math.sqrt(dx * dx + dy * dy);
                    double nextLength = currentLength + segmentLength;
                    if (nextLength >= targetLength) {
                        double localAlpha
                                = (currentLength - targetLength) / segmentLength;
                        //System.out.println("current "+currentLength+" target "+targetLength+" seg "+segmentLength);
                        double x = previous[0] + localAlpha * dx;
                        double y = previous[1] + localAlpha * dy;
                        return new Point2D.Double(x, y);
                    }
                    previous[0] = coords[0];
                    previous[1] = coords[1];
                    currentLength = nextLength;
                    break;
            }
            pi.next();
        }
        return null;
    }

    // This method is take from JUNG ShapePickSupport.java
    private <V, E> Shape getTransformedEdgeShape(
            VisualizationViewer<V, E> vv, Layout<V, E> layout, E e, V s) {
        Pair<V> pair = layout.getGraph().getEndpoints(e);
        V v1 = pair.getFirst();
        V v2 = pair.getSecond();
        boolean isLoop = v1.equals(v2);
        if (v2.equals(s)) {
            V tmp = v2;
            v2 = v1;
            v1 = tmp;
        }
        RenderContext<V, E> rc = vv.getRenderContext();
        MultiLayerTransformer multiLayerTransformer
                = rc.getMultiLayerTransformer();
        Point2D p1 = multiLayerTransformer.transform(
                Layer.LAYOUT, layout.transform(v1));
        Point2D p2 = multiLayerTransformer.transform(
                Layer.LAYOUT, layout.transform(v2));
        if (p1 == null || p2 == null) {
            return null;
        }
        float x1 = (float) p1.getX();
        float y1 = (float) p1.getY();
        float x2 = (float) p2.getX();
        float y2 = (float) p2.getY();
        AffineTransform xform = AffineTransform.getTranslateInstance(x1, y1);
        Shape edgeShape
                = rc.getEdgeShapeTransformer().transform(
                        Context.<Graph<V, E>, E>getInstance(
                                vv.getGraphLayout().getGraph(), e));
        if (isLoop) {
            Shape s2 = rc.getVertexShapeTransformer().transform(v2);
            Rectangle2D s2Bounds = s2.getBounds2D();
            xform.scale(s2Bounds.getWidth(), s2Bounds.getHeight());
            xform.translate(0, -edgeShape.getBounds2D().getHeight() / 2);
        } else {
            float dx = x2 - x1;
            float dy = y2 - y1;
            double theta = Math.atan2(dy, dx);
            xform.rotate(theta);
            float dist = (float) Math.sqrt(dx * dx + dy * dy);
            xform.scale(dist, 1.0f);
        }
        edgeShape = xform.createTransformedShape(edgeShape);
        return edgeShape;
    }

    private class CarLocation {

        public long elapsed = 0;
        public long total;
        public CarPainter carp;
        public Car c;

        CarLocation(Car ca, CarPainter c, long t) {
            carp = c;
            total = t;
            this.c = ca;
        }
    }
}
