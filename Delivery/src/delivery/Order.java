/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivery;

/**
 *
 * @author Krzysztof
 */
public class Order implements Comparable {

    private final String name;
    private final Vertex source;
    private final Vertex dest;
    private final short priority;
    private final int pathLength;
    private final int id;

    public Order(int id, Vertex source, Vertex dest, String name, short priority, int len) {
        this.id = id;
        this.source = source;
        this.dest = dest;
        this.name = name;
        this.priority = priority;
        this.pathLength = len;
    }

    @Override
    public int compareTo(Object t) {
        Order o = (Order) t;
        if (this.pathLength == o.pathLength) {
            return o.priority - this.priority;
        } else {
            return o.pathLength - this.pathLength;
        }
    }

    public Vertex getDest() {
        return dest;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Vertex getSource() {
        return source;
    }
}
