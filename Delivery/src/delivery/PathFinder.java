/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivery;

import java.util.ArrayList;

/**
 *
 * @author Krzysztof
 */
public interface PathFinder {
    public void init(Vertex source);
    public ArrayList<Vertex> getShortestPathTo(Vertex target);
}
