/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivery;

import java.util.ArrayList;

/**
 *
 * @author Krzysztof
 */
public class Vertex implements Comparable<Vertex> {

    private final int id;
    private final String name;
    private final ArrayList<Edge> adjacencies;
    private int minDistance = Integer.MAX_VALUE;
    private Vertex previous;

    public Vertex(String argName, int id) {
        name = argName;
        this.id = id;
        adjacencies = new ArrayList();
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Vertex other) {
        return minDistance - other.minDistance;
    }

    public int distanceTo(Vertex target) {
        for (Edge e : adjacencies) {
            if (e.target == target) {
                return e.weight;
            }
        }
        return 0;
    }

    public String getName() {
        return name;
    }

    public void addAdjacence(Edge edge) {
        adjacencies.add(edge);
    }

    public void setMinDistance(int d) {
        minDistance = d;
    }

    public ArrayList<Edge> getAdjacencies() {
        return adjacencies;
    }

    public int getMinDistance() {
        return minDistance;
    }

    public void setPreviousVertex(Vertex v) {
        previous = v;
    }

    public Vertex getPreviousVertex() {
        return previous;
    }
}
