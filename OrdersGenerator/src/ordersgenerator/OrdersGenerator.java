/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordersgenerator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Krzysztof
 */
public class OrdersGenerator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        if(args.length > 3){{
            try {
                File file = new File(args[0]);
 
		if (!file.exists()) 
                    file.createNewFile();
 		BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
                
                bw.write(args[2]);
                bw.write("\n");
                
		Random r = new Random();
                for(int i = 0; i < Integer.parseInt(args[1]); i++){
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.format("%03d", i));
                    sb.append(" ");
                    sb.append(args[2]);
                    sb.append(" ");
                    sb.append(r.nextInt(Integer.parseInt(args[3])));
                    sb.append(" ");
                    sb.append("Zamowienie");
                    sb.append(i);
                    sb.append(" ");
                    sb.append(r.nextInt(101));
                    sb.append("\n");
                    bw.write(sb.toString());
                }
		
                bw.close();
 
                } catch (IOException ex) {
                    Logger.getLogger(OrdersGenerator.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        }
    }
    
}
